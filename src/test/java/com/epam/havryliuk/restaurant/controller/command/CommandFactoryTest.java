package com.epam.havryliuk.restaurant.controller.command;

import com.epam.havryliuk.restaurant.controller.command.dishCommand.*;
import com.epam.havryliuk.restaurant.controller.command.orderCommand.*;
import com.epam.havryliuk.restaurant.controller.command.userCommand.LoginCommand;
import com.epam.havryliuk.restaurant.controller.command.userCommand.LoginPageCommand;
import com.epam.havryliuk.restaurant.controller.command.userCommand.LogoutCommand;
import com.epam.havryliuk.restaurant.controller.command.userCommand.RegisterCommand;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CommandFactoryTest {



    public static Stream<Arguments> testCases () {
        return Stream.of(
                Arguments.of ("/index", new IndexCommand()),
                Arguments.of ("/menu", new MenuCommand()),
                Arguments.of ("/show_dish_info", new DishInfoCommand()),
                Arguments.of ("/MAKE_ORDER", new MakeOrderCommand()),
                Arguments.of ("/BASKET", new BasketCommand()),
                Arguments.of ("/REMOVE_FROM_ORDER", new RemoveFromOrderCommand()),
                Arguments.of ("/SET_NEXT_STATUS", new SetNextStatusCommand()),
                Arguments.of ("/manage_orders", new ManageOrdersCommand()),
                Arguments.of ("/LOGIN", new LoginCommand()),
                Arguments.of ("/login_page", new LoginPageCommand()),
                Arguments.of ("/LOGOUT", new LogoutCommand()),
                Arguments.of ("/register", new RegisterCommand()),
                Arguments.of ("/ADD_DISH_PAGE", new AddDishPageCommand()),
                Arguments.of ("/ADD_DISH", new AddDishCommand()),
                Arguments.of ("/EDIT_DISH", new EditDishCommand())
        );
    }

    @Mock
    private HttpServletRequest request;

    @ParameterizedTest
    @MethodSource("testCases")
    void defineCommandSeries(String URI, Command command) {
        when(request.getServletPath()).thenReturn(URI);
        CommandFactory factory = new CommandFactory();
        assertEquals(command.getClass(), factory.defineCommand(request).getClass());
    }
}
