package com.epam.havryliuk.restaurant.controller.filters;

import com.epam.havryliuk.restaurant.controller.constants.paths.AppPagesPath;
import com.epam.havryliuk.restaurant.model.entity.Role;
import com.epam.havryliuk.restaurant.model.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.havryliuk.restaurant.controller.constants.RequestAttributes.LOGGED_USER;


@WebFilter(filterName = "AuthenticationManagerFilter",
        urlPatterns = { "/make_order", "/basket", "/remove_from_order"})
public class AuthenticationClientFilter implements Filter {
    private static final Logger LOG = LogManager.getLogger(AuthenticationClientFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        LOG.debug("\"/AuthenticationManagerFilter\" doFilter starts.");

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        User user = (User) httpRequest.getSession().getAttribute(LOGGED_USER);
        if (user != null && user.getRole() == Role.CLIENT) {
            chain.doFilter(request, response);
        } else {
            httpResponse.sendRedirect(AppPagesPath.REDIRECT_REGISTRATION);
        }
    }

    @Override
    public void destroy() {

    }

}
