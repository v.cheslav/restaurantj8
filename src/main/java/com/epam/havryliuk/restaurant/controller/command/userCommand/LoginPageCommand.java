package com.epam.havryliuk.restaurant.controller.command.userCommand;

import com.epam.havryliuk.restaurant.controller.command.Command;
import com.epam.havryliuk.restaurant.controller.constants.paths.AppPagesPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command forwards to "Login" page while User press the login menu button.
 */
public class LoginPageCommand implements Command {
    private static final Logger LOG = LogManager.getLogger(LoginPageCommand.class);

    /**
     * Method executes the command that forward user to "Login" page on correspondent button click.
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.trace("LoginPageCommand.");
        request.getRequestDispatcher(AppPagesPath.FORWARD_REGISTRATION).forward(request, response);
    }
}