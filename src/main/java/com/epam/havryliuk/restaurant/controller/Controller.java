package com.epam.havryliuk.restaurant.controller;

import com.epam.havryliuk.restaurant.controller.command.Command;
import com.epam.havryliuk.restaurant.controller.command.CommandFactory;
import com.epam.havryliuk.restaurant.controller.constants.paths.AppPagesPath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig
@WebServlet(name = "Controller", urlPatterns = {"/login_page", "/login", "/register", "/logout",
        "/index", "/menu/*", "/show_dish_info", "/make_order", "/basket", "/remove_from_order",
       "/set_next_status/*", "/manage_orders", "/add_dish_page", "/upload_picture", "/add_dish",
        "/edit_dish"})
public class Controller extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(Controller.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOG.trace("Controller, doGet.");
        processRequest(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        LOG.trace("Controller, doPost.");
        processRequest(request, response);
    }
    private void processRequest(HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        CommandFactory client = new CommandFactory();
        Command command = client.defineCommand(request);

        try {
            command.execute(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Unable to execute request.");
            response.sendRedirect(request.getContextPath() + AppPagesPath.REDIRECT_ERROR);
        }
    }
}