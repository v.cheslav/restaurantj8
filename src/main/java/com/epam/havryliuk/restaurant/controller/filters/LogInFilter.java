package com.epam.havryliuk.restaurant.controller.filters;

import com.epam.havryliuk.restaurant.controller.constants.paths.AppPagesPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.havryliuk.restaurant.controller.constants.RequestAttributes.LOGGED_USER;

@WebFilter(filterName = "LogInFilter",  urlPatterns = {"/show_dish_info", "/make_order",  "/basket",
        "/remove_from_order", "/set_next_status/*", "/manage_orders", "/add_dish_page", "/managerMenu", "/edit_dish"})
public class LogInFilter implements Filter {
    private static final Logger LOG = LogManager.getLogger(LogInFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        LOG.debug("\"/LogInFilter\" doFilter starts.");
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        if (!isUserLoggedIn(httpRequest)) {
            httpResponse.sendRedirect(AppPagesPath.REDIRECT_REGISTRATION);
        } else {
            chain.doFilter(request, response);
        }
    }

    private boolean isUserLoggedIn(HttpServletRequest httpRequest) {
        return httpRequest.getSession().getAttribute(LOGGED_USER) != null;
    }
    @Override
    public void destroy() {

    }
}
