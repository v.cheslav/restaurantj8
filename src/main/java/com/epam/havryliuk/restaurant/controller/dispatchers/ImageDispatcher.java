package com.epam.havryliuk.restaurant.controller.dispatchers;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;

public class ImageDispatcher {
    private static final Logger LOG = LogManager.getLogger(ImageDispatcher.class);
    private final Part part;
    private final String imageFileName;
    private final String realPath;

    public ImageDispatcher(HttpServletRequest request, String requestedParameter, String relativePath)
            throws ServletException, IOException {
        this.part = request.getPart(requestedParameter);
        this.imageFileName = part.getSubmittedFileName();
        this.realPath = request.getServletContext()
                .getRealPath(relativePath + imageFileName);
    }

    public Part getPart() {
        return part;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public String getRealPath() {
        return realPath;
    }
}
