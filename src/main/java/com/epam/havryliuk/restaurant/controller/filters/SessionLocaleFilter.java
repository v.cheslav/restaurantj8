package com.epam.havryliuk.restaurant.controller.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

import static com.epam.havryliuk.restaurant.controller.constants.RequestAttributes.LOCALE;

@WebFilter(filterName = "SessionLocaleFilter", urlPatterns = {"/*"})
public class SessionLocaleFilter implements Filter {
    private static final Logger LOG = LogManager.getLogger(SessionLocaleFilter.class);
    private static final Locale EN = new Locale("en", "EN");
    private static final Locale UA = new Locale("uk", "UA");

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        LOG.debug("\"/SessionLocaleFilter\" doFilter starts.");
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();
        String requestedLocale = httpRequest.getParameter(LOCALE);

        if (requestedLocale != null) {
            Locale locale;
            switch (requestedLocale) {
                case "EN" : locale = new Locale("en", "EN");
                    break;
                case "UA" : locale = new Locale("uk", "UA");
                    break;
                default : throw new IllegalArgumentException();
            }
            session.setAttribute(LOCALE, locale);
        } else if (session.getAttribute(LOCALE) == null) {
            session.setAttribute(LOCALE, Locale.getDefault());
            session.setAttribute(LOCALE, new Locale("en", "EN"));
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

}